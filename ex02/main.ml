let () =
    let methane = new Alkane.methane in
    let ethane = new Alkane.ethane in
    let propane = new Alkane.propane in
    let butane = new Alkane.butane in
    let pentane = new Alkane.pentane in
    let hexane = new Alkane.hexane in
    let heptane = new Alkane.heptane in
    let print_bool b = if b = true then " are equals" else " are not equals" in
    print_endline (methane#to_string ^ " and " ^ ethane#to_string ^ print_bool(methane#equals ethane));
    print_endline (propane#to_string ^ " and " ^ butane#to_string ^ print_bool(propane#equals butane));
    print_endline (pentane#to_string ^ " and " ^ hexane#to_string ^ print_bool(pentane#equals hexane));
    print_endline (heptane#to_string ^ " and " ^ heptane#to_string ^ print_bool(heptane#equals heptane))
