let () =
    let argon = new Atom.argon in
    let calcium = new Atom.calcium in
    let carbon = new Atom.carbon in
    let helium = new Atom.helium in
    let hydrogen = new Atom.hydrogen in
    let nitrogen = new Atom.nitrogen in
    let oxygen = new Atom.oxygen in
    let potassium = new Atom.potassium in
    let sodium = new Atom.sodium in
    let print_bool b = if b = true then " are equals" else " are not equals" in
    print_endline (argon#to_string ^ " and " ^ calcium#to_string ^ print_bool(argon#equals calcium));
    print_endline (carbon#to_string ^ " and " ^ helium#to_string ^ print_bool(carbon#equals helium));
    print_endline (hydrogen#to_string ^ " and " ^ nitrogen#to_string ^ print_bool(hydrogen#equals nitrogen));
    print_endline (oxygen#to_string ^ " and " ^ potassium#to_string ^ print_bool(oxygen#equals potassium));
    print_endline (sodium#to_string ^ " and " ^ sodium#to_string ^ print_bool(sodium#equals sodium))
