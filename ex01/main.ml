let () =
    let water = new Molecule.water in
    let carbon_diox = new Molecule.carbon_diox in
    let carbon_diat = new Molecule.carbon_diat in
    let hexa = new Molecule.hexa in
    let methane = new Molecule.methane in
    let ketene = new Molecule.ketene in
    let butadiyny = new Molecule.butadiyny in
    let print_bool b = if b = true then " are equals" else " are not equals" in
    print_endline (water#to_string ^ " and " ^ carbon_diox#to_string ^ print_bool(water#equals carbon_diox));
    print_endline (carbon_diat#to_string ^ " and " ^ hexa#to_string ^ print_bool(carbon_diat#equals hexa));
    print_endline (methane#to_string ^ " and " ^ ketene#to_string ^ print_bool(methane#equals ketene));
    print_endline (butadiyny#to_string ^ " and " ^ butadiyny#to_string ^ print_bool(butadiyny#equals butadiyny))
