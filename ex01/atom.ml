class virtual atom name symbol atomic_number =
    object
        val _name:string = name
        val _symbol:string = symbol
        val _atomic_number:int = atomic_number

        method name = _name
        method symbol = _symbol
        method atomic_number = _atomic_number

        method to_string = "Atom " ^ _name ^ ": " ^ _symbol ^ (string_of_int atomic_number)
        method equals x = if ((x :> atom)#name = _name) && ((x :> atom)#symbol = _symbol) && ((x :> atom)#atomic_number = atomic_number) then true else false
    end
    

class argon =
    object
        inherit atom "Argon" "Ar" 18
    end


class calcium =
    object
        inherit atom "Calcium" "Ca" 20
    end


class carbon =
    object
        inherit atom "Carbon" "C" 6
    end


class helium =
    object
        inherit atom "Helium" "He" 2
    end


class hydrogen =
    object
        inherit atom "Hydrogen" "H" 1
    end


class nitrogen =
    object
        inherit atom "Nitrogen" "N" 7
    end


class oxygen =
    object
        inherit atom "Oxygen" "O" 8
    end


class potassium =
    object
        inherit atom "Potassium" "K" 19
    end


class sodium =
    object
        inherit atom "Sodium" "Na" 11
    end
