class virtual molecule name formula =
    object
        val _name = name
        val _formula =
            let carbon_count = List.length (List.filter (fun x -> if x#symbol = "C" then true else false) formula) in
            let hydro_count = List.length (List.filter (fun x -> if x#symbol = "H" then true else false) formula) in
            let lst = List.filter (fun x -> if x#symbol = "C" || x#symbol = "H" then false else true) formula in
            let lst_sort = List.sort (fun x y -> String.compare x#symbol y#symbol) lst in
            let rec hill_not lst str =
                match lst with
                | []    ->  str
                | h::t  ->  let reff = h#symbol in
                            let count = List.length (List.filter (fun x -> if x#symbol = reff then true else false) t) in
                            let reff_num = if count > 0 then string_of_int (count + 1) else "" in
                            let new_str = str ^ reff ^ reff_num in
                            let new_lst = List.filter (fun x -> if x#symbol = reff then false else true) t in
                            hill_not new_lst new_str
            in
            let carbon_num = if carbon_count > 1 then string_of_int carbon_count else "" in
            let carbon = if carbon_count > 0 then "C" ^ carbon_num else "" in
            let hydro_num = if hydro_count > 1 then string_of_int hydro_count else "" in
            let hydro = if hydro_count > 0 then "H" ^ hydro_num else "" in
            hill_not lst_sort (carbon ^ hydro)

        method name = _name
        method formula = _formula
        method to_string = _name ^ ": " ^ _formula
        method equals x = if ((x :> molecule)#name = _name) && ((x :> molecule)#formula = _formula) then true else false
    end


class water =
    object
        inherit molecule "Water" [new Atom.hydrogen;new Atom.hydrogen;new Atom.oxygen]
    end


class carbon_diox =
    object
        inherit molecule "Carbon dioxyde" [new Atom.carbon;new Atom.oxygen;new Atom.oxygen]
    end


class carbon_diat =
    object
        inherit molecule "Diatomic carbon" [new Atom.carbon;new Atom.carbon;]
    end


class hexa =
    object
        inherit molecule "Hexapentaenylidene" [new Atom.hydrogen;new Atom.hydrogen;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;]
    end


class methane =
    object
        inherit molecule "Methane" [new Atom.carbon;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;]
    end


class ketene =
    object
        inherit molecule "Ketene" [new Atom.hydrogen;new Atom.hydrogen;new Atom.carbon;new Atom.carbon;new Atom.oxygen;]
    end


class butadiyny =
    object
        inherit molecule "Butadiyny" [new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.hydrogen;]
    end
