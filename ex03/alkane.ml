let name x = match x with
    |1 -> "Methane"
    |2 -> "Ethane"
    |3 -> "Propane"
    |4 -> "Butane"
    |5 -> "Pentane"
    |6 -> "Hexane"
    |7 -> "Heptane"
    |8 -> "Ocatane"
    |9 -> "Nonane"
    |10 -> "Decane"
    |11 -> "Gmalaucrane"
    |12 -> "Ousonmestatane"
    |13 -> "Bonetdane"
    | _ -> "unknow"

class virtual alkane n =
    object
        inherit Molecule.molecule (name n) ((Array.to_list (Array.make n (new Atom.carbon))) @ (Array.to_list (Array.make (2 * n + 2) (new Atom.hydrogen))))
    end


class methane =
    object
        inherit alkane 1
    end


class ethane =
    object
        inherit alkane 2
    end


class propane =
    object
        inherit alkane 3
    end


class butane =
    object
        inherit alkane 4
    end


class pentane =
    object
        inherit alkane 5
    end


class hexane =
    object
        inherit alkane 6
    end


class heptane =
    object
        inherit alkane 7
    end
